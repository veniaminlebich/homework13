import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.text.Format;
import java.text.SimpleDateFormat;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import javafx.scene.image.Image;


public class WxModel {
	
	JsonElement jsonData;
	JsonObject main;
	JsonObject wind;
	JsonArray weather;
	String zip;
	
	public int OWMApiReq(String zipCode) {
		zip = zipCode;
		String apiUrl = "http://api.openweathermap.org/data/2.5/weather?zip="+ zipCode +",us&units=imperial&appid=20b76a9510afdbcad7687660f85cfa35";

		try
		{
			// Construct API URL
			URL owmURL = new URL(apiUrl);

			// Open the URL
			InputStream is = owmURL.openStream(); // throws an IOException
			BufferedReader br = new BufferedReader(new InputStreamReader(is));

			// Read the result into a JSON Element
			jsonData = new JsonParser().parse(br);
      
			// Close the connection
			is.close();
			br.close();
		}
		
		catch (java.io.UnsupportedEncodingException uee)
		{
			uee.printStackTrace();
		}
		catch (java.net.MalformedURLException mue)
		{
			mue.printStackTrace();
		}
		catch (java.io.IOException ioe)
		{
			System.out.println("ERROR: No cities match your search query");
		}
		
		if (statusCode() == 200) {
			main = jsonData.getAsJsonObject().get("main").getAsJsonObject();
		    wind = jsonData.getAsJsonObject().get("wind").getAsJsonObject();
		    weather = jsonData.getAsJsonObject().get("weather").getAsJsonArray();

		}
		
		return statusCode();
		
	}
	
	public int statusCode() {
		if (jsonData != null) {
			return jsonData.getAsJsonObject().get("cod").getAsInt();
		} else {
			return 400;
		}
		
	}
	
	public String getZip() {
		return zip;
	}
	
	public String getCity() {
		return jsonData.getAsJsonObject().get("name").getAsString();
	}
	
	public String getTime() {
		Format formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		int timeStamp = jsonData.getAsJsonObject().get("dt").getAsInt();
		java.util.Date date = new java.util.Date((long)timeStamp*1000);
		 return formatter.format(date);
	}
	
	public String getWeather() {
		return weather.get(0).getAsJsonObject().get("main").getAsString();
	}
	
	public String getVisibility() {
		return weather.get(0).getAsJsonObject().get("description").getAsString();
	}
	
	public double getTemp() {
		return main.getAsJsonObject().get("temp").getAsDouble();
	}
	
	public double getWindSpeed() {
		return wind.getAsJsonObject().get("speed").getAsDouble();
	}
	
	public String getWindDirect() {
		double deg = wind.getAsJsonObject().get("deg").getAsDouble();
		deg = deg % 360; // make deg 0-360
		String[] points = {"N", "NE", "E", "SE", "S", "SW", "W", "NW"};
		if (deg > 337.5 || deg <= 22.5) { // N
			return points[0];
		} else if (deg > 22.5 && deg <= 67.5) { // NE
			return points[1];
		} else if (deg > 67.5 && deg <= 112.5) { // E
			return points[2];
		} else if (deg > 112.5 && deg <= 157.5) { // SE
			return points[3];
		} else if (deg > 157.5 && deg <= 202.5) { // S
			return points[4];
		} else if (deg > 202.5 && deg <= 247.5) { // SW
			return points[5];
		} else if (deg > 247.5 && deg <= 292.5) { // W
			return points[6];
		} else if (deg > 292.5 && deg <= 337.5) { // NW
			return points[7];
		} else {
			return "Error with deg";
		}
	}
	
	public double getPressure() {
		return (main.getAsJsonObject().get("pressure").getAsDouble()/3386.389)*100;
	}

	public double getHumidity() {
		return main.getAsJsonObject().get("humidity").getAsDouble();
	}
	
	public Image getIcon() {
		String iconURL = "http://openweathermap.org/img/w/" + weather.get(0).getAsJsonObject().get("icon").getAsString() + ".png";
		System.out.println(iconURL);
	    return new Image(iconURL);
	}

	
}